OpenAdvertMap
=============

![OpenAdvertMap logo](./src/img/logo.png)

Read-me
-------

OpenAdvertMap is an interactive map showing [advertisement-related data](http://wiki.openstreetmap.org/wiki/Key:advertising) from [OpenStreetMap](http://openstreetmap.org). OpenAdvertMap is written in JavaScript (ES6).

A live demo is available at: https://openadvertmap.pavie.info/


Build
-----

### Dependencies
* npm
* sed
* inkscape

### Compiling

If you want to build OpenAdvertMap by yourself, you can do the following (but it's not necessary for install, as a build is already available in [Tags page](https://framagit.org/PanierAvide/OpenAdvertMap/tags)):
```
make
```
When this is done, OpenAdvertMap is ready in **dist/** folder.


Installation
------------

If you want to install your own OpenAdvertMap instance, just upload the content of the **dist/** folder after build (or of **dist.zip**) in your own FTP or web server. That's all.


License
-------

Copyright 2016 Adrien PAVIE

See LICENSE for complete AGPL3 license.

OpenAdvertMap is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenAdvertMap is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenAdvertMap. If not, see <http://www.gnu.org/licenses/>.
