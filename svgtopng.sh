#!/bin/bash

for i in `ls $1/*.svg`; do
	n=$(basename $i)

	inkscapeVersion=$(inkscape --version | head -n1 | cut -d ' ' -f 2 | cut -d '.' -f 1)
	if [ "$inkscapeVersion" -eq "0" ]; then
		inkscape -e $2/${n%.svg}.png $i
	else
		inkscape -o $2/${n%.svg}.png $i
	fi
done
