# OpenAdvertMap
# Interactive map showing advertisement data from OpenStreetMap
# Author: Adrien PAVIE
#
# Makefile

# Folders
SRC_FOLDER=./src
DIST_FOLDER=./dist
NM_FOLDER=./node_modules
IMG_FOLDER=$(SRC_FOLDER)/img

# Binaries
BROWSERIFY=$(NM_FOLDER)/browserify/bin/cmd.js
NPMCSS=$(NM_FOLDER)/npm-css/bin/npm-css
UGLIFYCSS=$(NM_FOLDER)/uglifycss/uglifycss
UGLIFYJS=$(NM_FOLDER)/uglify-js-harmony/bin/uglifyjs
WATCHIFY=$(NM_FOLDER)/watchify/bin/cmd.js

# Options
BABELIFY_TRANSFORM=-t [ babelify --presets [ latest stage-0 ] ]

# Targets
CSS_OUT=$(DIST_FOLDER)/OAM.css
CSS_MIN_OUT=$(DIST_FOLDER)/OAM.min.css
DIST_ZIP=./dist.zip
MAIN_JS_OUT=$(DIST_FOLDER)/OAM.js
MAIN_JS_MIN_OUT=$(DIST_FOLDER)/OAM.min.js


# Tasks
all: deps assets jsdebug js distzip

deps:
	npm install

assets: distfolder html images nm_assets css mincss

distfolder:
	mkdir -p $(DIST_FOLDER)

html:
	cp $(SRC_FOLDER)/*.html $(DIST_FOLDER)/
	cp $(SRC_FOLDER)/*.json $(DIST_FOLDER)/

images:
	mkdir -p $(DIST_FOLDER)/img
	./svgtopng.sh $(IMG_FOLDER) $(DIST_FOLDER)/img

nm_assets:
	cp $(NM_FOLDER)/leaflet/dist/images/* $(DIST_FOLDER)/img/
	cp $(NM_FOLDER)/leaflet-geosearch/src/img/* $(DIST_FOLDER)/img/
	cp -r $(NM_FOLDER)/font-awesome/fonts $(DIST_FOLDER)/

css:
	$(NPMCSS) $(SRC_FOLDER)/css/OAM.css > $(CSS_OUT)
	sed -i 's#images/#img/#g;s#../img/#img/#g;s#../fonts/#fonts/#g' $(CSS_OUT)

mincss:
	$(UGLIFYCSS) --ugly-comments $(CSS_OUT) > $(CSS_MIN_OUT)

jsdebug:
	$(BROWSERIFY) $(BABELIFY_TRANSFORM) $(SRC_FOLDER)/js/ctrl/Main.js --debug --s OAM > $(MAIN_JS_OUT)

js:
	$(BROWSERIFY) $(SRC_FOLDER)/js/ctrl/Main.js --s OAM $(BABELIFY_TRANSFORM)  | $(UGLIFYJS) -c > $(MAIN_JS_MIN_OUT)

distzip:
	rm -f $(DIST_ZIP)
	zip -9 -r $(DIST_ZIP) $(DIST_FOLDER)/


# Watch
watchmain:
	$(WATCHIFY) --debug $(SRC_FOLDER)/js/ctrl/Main.js -o $(MAIN_JS_OUT) --s OAM -v $(BABELIFY_TRANSFORM)


# Cleaning
clean:
	rm -rf $(DIST_FOLDER)
	rm -f $(DIST_ZIP)
